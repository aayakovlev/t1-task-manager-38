package ru.t1.aayakovlev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.aayakovlev.tm.dto.request.*;
import ru.t1.aayakovlev.tm.dto.response.UserLogoutResponse;
import ru.t1.aayakovlev.tm.dto.response.UserShowProfileResponse;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.marker.IntegrationCategory;
import ru.t1.aayakovlev.tm.service.PropertyService;
import ru.t1.aayakovlev.tm.service.impl.PropertyServiceImpl;

import static ru.t1.aayakovlev.tm.constant.UserEndpointTestConstant.*;

@Category(IntegrationCategory.class)
public final class AuthEndpointImplTest {

    @NotNull
    private final PropertyService propertyService = new PropertyServiceImpl();

    @NotNull
    private final AuthEndpoint authEndpoint = AuthEndpoint.newInstance(propertyService);

    @NotNull
    private final UserEndpoint userEndpoint = UserEndpoint.newInstance(propertyService);

    @Nullable
    private String adminToken;

    @Before
    public void init() throws AbstractException {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin(ADMIN_LOGIN);
        loginRequest.setPassword(ADMIN_PASSWORD);
        adminToken = authEndpoint.login(loginRequest).getToken();
        @NotNull final UserRegistryRequest request = new UserRegistryRequest(adminToken);
        request.setLogin(TEST_LOGIN);
        request.setPassword(TEST_PASSWORD);
        request.setEmail(TEST_EMAIL);
        userEndpoint.registry(request);
    }

    @After
    public void after() throws AbstractException {

        @NotNull final UserRemoveRequest request = new UserRemoveRequest(adminToken);
        request.setLogin(TEST_LOGIN);
        userEndpoint.remove(request);
    }

    @NotNull
    private String getToken() throws AbstractException {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin(TEST_LOGIN);
        loginRequest.setPassword(TEST_PASSWORD);
        return authEndpoint.login(loginRequest).getToken();
    }

    @Test
    public void When_Login_Expect_UserLoginResponse() throws AbstractException {
        @Nullable final String token = getToken();
        Assert.assertNotNull(token);
    }

    @Test
    public void When_Logout_Expect_UserLogoutResponse() throws AbstractException {
        @Nullable final String token = getToken();

        @NotNull final UserLogoutRequest request = new UserLogoutRequest(token);
        @Nullable final UserLogoutResponse response = authEndpoint.logout(request);
        Assert.assertNotNull(response);
    }

    @Test
    public void When_Profile_Expect_UserShowProfileResponse() throws AbstractException {
        @Nullable final String token = getToken();

        @NotNull final UserShowProfileRequest request = new UserShowProfileRequest(token);
        @Nullable final UserShowProfileResponse response = authEndpoint.profile(request);

        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getUser());
        Assert.assertEquals(TEST_LOGIN, response.getUser().getLogin());
    }

}
