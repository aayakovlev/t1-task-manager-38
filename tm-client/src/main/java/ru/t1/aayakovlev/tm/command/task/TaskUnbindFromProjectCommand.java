package ru.t1.aayakovlev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.request.TaskUnbindFromProjectRequest;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Unbind task from project.";

    @NotNull
    public static final String NAME = "task-unbind-from-project";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[UNBIND TASK TO PROJECT]");
        System.out.print("Enter project id: ");
        @NotNull final String projectId = nextLine();
        System.out.print("Enter task id: ");
        @NotNull final String taskId = nextLine();

        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(getToken());
        request.setTaskId(taskId);
        request.setProjectId(projectId);

        getTaskEndpoint().unbindFromProject(request);
    }

}
