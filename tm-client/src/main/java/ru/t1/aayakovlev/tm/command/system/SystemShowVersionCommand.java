package ru.t1.aayakovlev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.dto.request.ServerVersionRequest;
import ru.t1.aayakovlev.tm.dto.response.ServerVersionResponse;

public final class SystemShowVersionCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = "-v";

    @NotNull
    public static final String DESCRIPTION = "Show application version.";

    @NotNull
    public static final String NAME = "version";

    @Override
    @NotNull
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");

        @NotNull final ServerVersionRequest request = new ServerVersionRequest();
        @Nullable final ServerVersionResponse response = getSystemEndpoint().getVersion(request);

        System.out.println(response.getVersion());
    }

}
