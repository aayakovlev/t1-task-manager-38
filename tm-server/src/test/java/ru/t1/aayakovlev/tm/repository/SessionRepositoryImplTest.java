package ru.t1.aayakovlev.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;
import ru.t1.aayakovlev.tm.exception.field.UserIdEmptyException;
import ru.t1.aayakovlev.tm.marker.UnitCategory;
import ru.t1.aayakovlev.tm.model.Session;
import ru.t1.aayakovlev.tm.repository.impl.SessionRepositoryImpl;
import ru.t1.aayakovlev.tm.service.ConnectionService;
import ru.t1.aayakovlev.tm.service.PropertyService;
import ru.t1.aayakovlev.tm.service.SessionService;
import ru.t1.aayakovlev.tm.service.impl.ConnectionServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.PropertyServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.SessionServiceImpl;

import java.sql.Connection;
import java.util.Comparator;
import java.util.List;

import static ru.t1.aayakovlev.tm.constant.SessionTestConstant.*;
import static ru.t1.aayakovlev.tm.constant.TaskTestConstant.USER_TASK_LIST;
import static ru.t1.aayakovlev.tm.constant.UserTestConstant.ADMIN_USER_ONE;
import static ru.t1.aayakovlev.tm.constant.UserTestConstant.COMMON_USER_ONE;

@Category(UnitCategory.class)
public final class SessionRepositoryImplTest {

    @NotNull
    private static final PropertyService propertyService = new PropertyServiceImpl();

    @NotNull
    private static final ConnectionService connectionService = new ConnectionServiceImpl(propertyService);

    @NotNull
    private static final Connection connection = connectionService.getConnection();

    @NotNull
    private static final SessionRepository repository = new SessionRepositoryImpl(connectionService.getConnection());

    @NotNull
    private static final SessionService service = new SessionServiceImpl(connectionService);

    @Before
    public void init() throws EntityEmptyException {
        service.save(SESSION_USER_ONE);
        service.save(SESSION_USER_TWO);
    }

    @After
    public void after() {
        service.removeAll(SESSION_LIST);
    }

    @Test
    public void When_FindByIdExistsSession_Expect_ReturnProject() throws AbstractException {
        final @Nullable Session project = repository.findById(COMMON_USER_ONE.getId(), SESSION_USER_ONE.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(SESSION_USER_ONE.getUserId(), project.getUserId());
    }

    @Test
    public void When_FindByIdExistsSession_Expect_ReturnNull() throws AbstractException {
        @Nullable final Session session = repository.findById(SESSION_ID_NOT_EXISTED);
        Assert.assertNull(session);
    }

    @Test
    @SneakyThrows
    public void When_SaveNotNullSession_Expect_ReturnProject() {
        @NotNull final Session savedSession = repository.save(ADMIN_USER_ONE.getId(), SESSION_ADMIN_ONE);
        connection.commit();
        Assert.assertNotNull(savedSession);
        Assert.assertEquals(SESSION_ADMIN_ONE.getUserId(), savedSession.getUserId());
        @Nullable final Session session = repository.findById(ADMIN_USER_ONE.getId(), SESSION_ADMIN_ONE.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(SESSION_ADMIN_ONE.getUserId(), session.getUserId());
    }

    @Test
    public void When_CountCommonUserSessions_Expect_ReturnTwo() throws UserIdEmptyException {
        final int count = repository.count(COMMON_USER_ONE.getId());
        Assert.assertEquals(2, count);
    }

    @Test
    public void When_ExistsByIdExistedSession_Expected_ReturnTrue() throws AbstractFieldException {
        final boolean exists = repository.existsById(SESSION_USER_ONE.getId());
        Assert.assertTrue(exists);
    }

    @Test
    public void When_ExistsByIdNotExistedSession_Expected_ReturnFalse() throws AbstractFieldException {
        final boolean exists = repository.existsById(SESSION_ID_NOT_EXISTED);
        Assert.assertFalse(exists);
    }

    @Test
    public void When_ExistsByIdWithUserIdExistedSession_Expected_ReturnTrue() throws AbstractFieldException {
        final boolean exists = repository.existsById(COMMON_USER_ONE.getId(), SESSION_USER_ONE.getId());
        Assert.assertTrue(exists);
    }

    @Test
    public void When_ExistsByIdWithUserIdNotExistedSession_Expected_ReturnFalse() throws AbstractFieldException {
        final boolean exists = repository.existsById(COMMON_USER_ONE.getId(), SESSION_ID_NOT_EXISTED);
        Assert.assertFalse(exists);
    }

    @Test
    public void When_FindAllUserId_Expected_ReturnListSessions() throws UserIdEmptyException {
        @NotNull final List<Session> sessions = repository.findAll(COMMON_USER_ONE.getId());
        for (int i = 0; i < sessions.size(); i++) {
            Assert.assertEquals(USER_SESSION_LIST.get(i).getId(), sessions.get(i).getId());
        }
    }

    @Test
    public void When_FindAllUserIdSorted_Expect_ReturnSortedSessionList() throws UserIdEmptyException {
        @NotNull final List<Session> sessions = repository.findAll(
                COMMON_USER_ONE.getId(),
                Comparator.comparing(Session::getId)
        );
        for (int i = 0; i < sessions.size(); i++) {
            Assert.assertEquals(USER_SESSION_SORTED_LIST.get(i).getId(), sessions.get(i).getId());
        }
    }

    @Test
    @SneakyThrows
    public void When_RemoveExistedSession_Expect_ReturnSession() {
        @Nullable Session session1 = repository.save(ADMIN_USER_ONE.getId(), SESSION_ADMIN_THREE);
        connection.commit();
        Assert.assertNotNull(session1);
        @Nullable Session session2 = repository.remove(ADMIN_USER_ONE.getId(), SESSION_ADMIN_THREE);
        connection.commit();
        Assert.assertNotNull(session2);
    }

    @Test
    public void When_RemoveNotSession_Expect_ThrowsEntityNotFoundException() {
        Assert.assertThrows(
                EntityNotFoundException.class,
                () -> repository.remove(ADMIN_USER_ONE.getId(), SESSION_NOT_EXISTED)
        );
    }

    @Test
    @SneakyThrows
    public void When_RemoveAll_Expect_ZeroCountSessions() {
        repository.removeAll(ADMIN_USER_ONE.getId());
        connection.commit();
        Assert.assertEquals(0, repository.count(ADMIN_USER_ONE.getId()));
    }

    @Test
    @SneakyThrows
    public void When_RemoveByIdExistedSession_Expect_Session() {
        @Nullable Session session1 = repository.save(ADMIN_USER_ONE.getId(), SESSION_ADMIN_TWO);
        connection.commit();
        Assert.assertNotNull(session1);
        @Nullable Session session2 = repository.removeById(ADMIN_USER_ONE.getId(), SESSION_ADMIN_TWO.getId());
        connection.commit();
        Assert.assertNotNull(session2);
    }

    @Test
    public void When_RemoveByIdNotExistedSession_Expect_ThrowsEntityNotFoundException() {
        Assert.assertThrows(
                EntityNotFoundException.class,
                () -> repository.removeById(ADMIN_USER_ONE.getId(), SESSION_NOT_EXISTED.getId())
        );
    }

}
