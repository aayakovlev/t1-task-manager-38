package ru.t1.aayakovlev.tm.constant;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.model.Session;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static ru.t1.aayakovlev.tm.constant.UserTestConstant.ADMIN_USER_ONE;
import static ru.t1.aayakovlev.tm.constant.UserTestConstant.COMMON_USER_ONE;

public final class SessionTestConstant {

    @NotNull
    public final static Session SESSION_USER_ONE = new Session();

    @NotNull
    public final static Session SESSION_USER_TWO = new Session();

    @NotNull
    public final static Session SESSION_ADMIN_ONE = new Session();

    @NotNull
    public final static Session SESSION_ADMIN_TWO = new Session();

    @NotNull
    public final static Session SESSION_ADMIN_THREE = new Session();

    @NotNull
    public final static Session SESSION_NOT_EXISTED = new Session();

    @NotNull
    public final static String SESSION_ID_NOT_EXISTED = SESSION_NOT_EXISTED.getId();

    @NotNull
    public final static List<Session> ADMIN_SESSION_LIST = Arrays.asList(
            SESSION_ADMIN_ONE, SESSION_ADMIN_TWO, SESSION_ADMIN_THREE
    );

    @NotNull
    public final static List<Session> USER_SESSION_LIST = Arrays.asList(SESSION_USER_ONE, SESSION_USER_TWO);

    @NotNull
    public final static List<Session> USER_SESSION_SORTED_LIST = new ArrayList<>(USER_SESSION_LIST);

    @NotNull
    public final static List<Session> SESSION_LIST = new ArrayList<>();

    @NotNull
    public final static List<Session> SESSION_SORTED_LIST = new ArrayList<>();

    static {
        ADMIN_SESSION_LIST.forEach(
                session -> {
                    session.setUserId(ADMIN_USER_ONE.getId());
                }
        );
        USER_SESSION_LIST.forEach(
                session -> {
                    session.setUserId(COMMON_USER_ONE.getId());
                }
        );

        SESSION_LIST.addAll(ADMIN_SESSION_LIST);
        SESSION_LIST.addAll(USER_SESSION_LIST);

        USER_SESSION_SORTED_LIST.sort(Comparator.comparing(Session::getId));

        SESSION_SORTED_LIST.addAll(SESSION_LIST);
        SESSION_SORTED_LIST.sort(Comparator.comparing(Session::getId));
    }

}
