package ru.t1.aayakovlev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;
import ru.t1.aayakovlev.tm.marker.UnitCategory;
import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.service.impl.*;

import static ru.t1.aayakovlev.tm.constant.ProjectTestConstant.*;
import static ru.t1.aayakovlev.tm.constant.TaskTestConstant.*;
import static ru.t1.aayakovlev.tm.constant.UserTestConstant.ADMIN_USER_ONE;
import static ru.t1.aayakovlev.tm.constant.UserTestConstant.ADMIN_USER_TWO;

@Category(UnitCategory.class)
public final class ProjectTaskServiceImplTest {

    @NotNull
    private static final PropertyService propertyService = new PropertyServiceImpl();

    @NotNull
    private static final ConnectionService connectionService = new ConnectionServiceImpl(propertyService);

    @NotNull
    private static final ProjectService projectService = new ProjectServiceImpl(connectionService);

    @NotNull
    private static final TaskService taskService = new TaskServiceImpl(connectionService);

    @NotNull
    private final ProjectTaskService service = new ProjectTaskServiceImpl(connectionService);

    @BeforeClass
    public static void init() throws EntityEmptyException {
        projectService.save(PROJECT_USER_ONE);
        projectService.save(PROJECT_USER_TWO);
        taskService.save(TASK_USER_ONE);
        taskService.save(TASK_USER_TWO);
    }

    @After
    public void after() {
        projectService.removeAll(PROJECT_LIST);
        taskService.removeAll(TASK_LIST);
    }

    @Test
    public void When_BindTaskToProject_Expect_TaskWithProjectId() throws AbstractException {
        projectService.save(PROJECT_ADMIN_ONE);
        taskService.save(TASK_EMPTY_PROJECT_ID);
        @NotNull final Task task = service.bindTaskToProject(
                ADMIN_USER_ONE.getId(), PROJECT_ADMIN_ONE.getId(), TASK_EMPTY_PROJECT_ID.getId()
        );
        Assert.assertNotNull(task.getProjectId());
    }

    @Test
    public void When_UnbindTaskFromProject_Expect_TaskWithoutProjectId() throws AbstractException {
        projectService.save(PROJECT_ADMIN_ONE);
        taskService.save(TASK_ADMIN_ONE);
        @NotNull final Task task = service.unbindTaskFromProject(
                ADMIN_USER_ONE.getId(), PROJECT_ADMIN_ONE.getId(), TASK_ADMIN_ONE.getId()
        );
        Assert.assertNull(task.getProjectId());
    }

    @Test
    public void When_RemoveProjectById_Expect_ThrowsEntityNotFoundException() throws AbstractException {
        projectService.save(PROJECT_ADMIN_TWO);
        taskService.save(TASK_ADMIN_ONE);
        taskService.save(TASK_ADMIN_TWO);
        Assert.assertTrue(taskService.findByProjectId(ADMIN_USER_TWO.getId(), PROJECT_ADMIN_TWO.getId()).isEmpty());
    }

}
