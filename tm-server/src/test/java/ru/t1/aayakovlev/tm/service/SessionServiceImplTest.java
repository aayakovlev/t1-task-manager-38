package ru.t1.aayakovlev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;
import ru.t1.aayakovlev.tm.exception.field.UserIdEmptyException;
import ru.t1.aayakovlev.tm.marker.UnitCategory;
import ru.t1.aayakovlev.tm.model.Session;
import ru.t1.aayakovlev.tm.service.impl.ConnectionServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.PropertyServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.SessionServiceImpl;

import java.util.Comparator;
import java.util.List;

import static ru.t1.aayakovlev.tm.constant.SessionTestConstant.*;
import static ru.t1.aayakovlev.tm.constant.UserTestConstant.ADMIN_USER_ONE;
import static ru.t1.aayakovlev.tm.constant.UserTestConstant.COMMON_USER_ONE;

@Category(UnitCategory.class)
public final class SessionServiceImplTest {

    @NotNull
    private static final PropertyService propertyService = new PropertyServiceImpl();

    @NotNull
    private static final ConnectionService connectionService = new ConnectionServiceImpl(propertyService);

    @NotNull
    private static final SessionService service = new SessionServiceImpl(connectionService);

    @BeforeClass
    public static void init() throws EntityEmptyException {
        service.save(SESSION_USER_ONE);
        service.save(SESSION_USER_TWO);
    }

    @AfterClass
    public static void after() {
        service.removeAll(SESSION_LIST);
    }

    @Test
    public void When_FindByIdExistsSESSION_Expect_ReturnSession() throws AbstractException {
        @Nullable final Session session = service.findById(COMMON_USER_ONE.getId(), SESSION_USER_ONE.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(SESSION_USER_ONE.getUserId(), session.getUserId());
    }

    @Test
    public void When_FindByIdExistsSESSION_Expect_ThrowsEntityNotFoundException() {
        Assert.assertThrows(EntityNotFoundException.class, () -> service.findById(SESSION_ID_NOT_EXISTED));

    }

    @Test
    public void When_SaveNotNullSESSION_Expect_ReturnSession() throws AbstractException {
        @NotNull final Session savedSession = service.save(ADMIN_USER_ONE.getId(), SESSION_ADMIN_ONE);
        Assert.assertNotNull(savedSession);
        Assert.assertEquals(SESSION_ADMIN_ONE, savedSession);
        @Nullable final Session session = service.findById(ADMIN_USER_ONE.getId(), SESSION_ADMIN_ONE.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(SESSION_ADMIN_ONE, session);
    }

    @Test
    public void When_CountCommonUserSessions_Expect_ReturnTwo() throws AbstractException {
        final int count = service.count(COMMON_USER_ONE.getId());
        Assert.assertEquals(2, count);
    }

    @Test
    public void When_ExistsByIdExistedSession_Expected_ReturnTrue() throws AbstractFieldException {
        final boolean exists = service.existsById(SESSION_USER_ONE.getId());
        Assert.assertTrue(exists);
    }

    @Test
    public void When_ExistsByIdNotExistedSession_Expected_ReturnFalse() throws AbstractFieldException {
        final boolean exists = service.existsById(SESSION_ID_NOT_EXISTED);
        Assert.assertFalse(exists);
    }

    @Test
    public void When_ExistsByIdWithUserIdExistedSession_Expected_ReturnTrue() throws AbstractFieldException {
        final boolean exists = service.existsById(COMMON_USER_ONE.getId(), SESSION_USER_ONE.getId());
        Assert.assertTrue(exists);
    }

    @Test
    public void When_ExistsByIdWithUserIdNotExistedSession_Expected_ReturnFalse() throws AbstractFieldException {
        final boolean exists = service.existsById(COMMON_USER_ONE.getId(), SESSION_ID_NOT_EXISTED);
        Assert.assertFalse(exists);
    }

    @Test
    public void When_FindAllUserId_Expected_ReturnSessionList() throws UserIdEmptyException {
        final List<Session> sessions = service.findAll(COMMON_USER_ONE.getId());
        Assert.assertArrayEquals(sessions.toArray(), USER_SESSION_LIST.toArray());
    }

    @Test
    public void When_FindAllUserIdSorted_Expect_ReturnSortedSessionList() throws UserIdEmptyException {
        @NotNull final List<Session> sessions = service.findAll(
                COMMON_USER_ONE.getId(), Comparator.comparing(Session::getId)
        );
        Assert.assertArrayEquals(sessions.toArray(), USER_SESSION_SORTED_LIST.toArray());
    }

    @Test
    public void When_RemoveExistedSession_Expect_ReturnSession() throws AbstractException {
        Assert.assertNotNull(service.save(ADMIN_USER_ONE.getId(), SESSION_ADMIN_TWO));
        Assert.assertNotNull(service.remove(ADMIN_USER_ONE.getId(), SESSION_ADMIN_TWO));
    }

    @Test
    public void When_RemoveNotSession_Expect_ThrowsEntityNotFoundException() {
        Assert.assertThrows(
                EntityNotFoundException.class,
                () -> service.remove(ADMIN_USER_ONE.getId(), SESSION_NOT_EXISTED)
        );
    }

    @Test
    public void When_RemoveAll_Expect_ZeroCountSessions() throws AbstractException {
        service.save(SESSION_ADMIN_ONE);
        service.save(SESSION_ADMIN_TWO);
        service.removeAll(ADMIN_USER_ONE.getId());
        Assert.assertEquals(0, service.count(ADMIN_USER_ONE.getId()));
    }

    @Test
    public void When_RemoveByIdExistedSession_Expect_Session() throws AbstractException {
        Assert.assertNotNull(service.save(ADMIN_USER_ONE.getId(), SESSION_ADMIN_TWO));
        Assert.assertNotNull(service.removeById(ADMIN_USER_ONE.getId(), SESSION_ADMIN_TWO.getId()));
    }

    @Test
    public void When_RemoveByIdNotExistedSession_Expect_ThrowsEntityNotFoundException() {
        Assert.assertThrows(
                EntityNotFoundException.class,
                () -> service.removeById(ADMIN_USER_ONE.getId(), SESSION_NOT_EXISTED.getId())
        );
    }

}
