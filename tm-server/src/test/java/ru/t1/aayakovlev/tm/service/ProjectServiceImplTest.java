package ru.t1.aayakovlev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.aayakovlev.tm.enumerated.Sort;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;
import ru.t1.aayakovlev.tm.exception.field.UserIdEmptyException;
import ru.t1.aayakovlev.tm.marker.UnitCategory;
import ru.t1.aayakovlev.tm.model.Project;
import ru.t1.aayakovlev.tm.service.impl.ConnectionServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.ProjectServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.PropertyServiceImpl;

import java.util.List;

import static ru.t1.aayakovlev.tm.constant.ProjectTestConstant.*;
import static ru.t1.aayakovlev.tm.constant.UserTestConstant.ADMIN_USER_ONE;
import static ru.t1.aayakovlev.tm.constant.UserTestConstant.COMMON_USER_ONE;

@Category(UnitCategory.class)
public final class ProjectServiceImplTest {


    @NotNull
    private static final PropertyService propertyService = new PropertyServiceImpl();

    @NotNull
    private static final ConnectionService connectionService = new ConnectionServiceImpl(propertyService);

    @NotNull
    private static final ProjectService service = new ProjectServiceImpl(connectionService);

    @BeforeClass
    public static void init() throws EntityEmptyException {
        service.save(PROJECT_USER_ONE);
        service.save(PROJECT_USER_TWO);
    }

    @AfterClass
    public static void after() {
        service.removeAll(PROJECT_LIST);
    }

    @Test
    public void When_FindByIdExistsProject_Expect_ReturnProject() throws AbstractException {
        @Nullable final Project project = service.findById(COMMON_USER_ONE.getId(), PROJECT_USER_ONE.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(PROJECT_USER_ONE.getDescription(), project.getDescription());
        Assert.assertEquals(PROJECT_USER_ONE.getName(), project.getName());
        Assert.assertEquals(PROJECT_USER_ONE.getStatus(), project.getStatus());
        Assert.assertEquals(PROJECT_USER_ONE.getUserId(), project.getUserId());
        Assert.assertEquals(PROJECT_USER_ONE.getCreated(), project.getCreated());
    }

    @Test
    public void When_FindByIdExistsProject_Expect_ThrowsEntityNotFoundException() {
        Assert.assertThrows(EntityNotFoundException.class, () -> service.findById(PROJECT_ID_NOT_EXISTED));

    }

    @Test
    public void When_SaveNotNullProject_Expect_ReturnProject() throws AbstractException {
        @NotNull final Project savedProject = service.save(ADMIN_USER_ONE.getId(), PROJECT_ADMIN_ONE);
        Assert.assertNotNull(savedProject);
        Assert.assertEquals(PROJECT_ADMIN_ONE, savedProject);
        @Nullable final Project project = service.findById(ADMIN_USER_ONE.getId(), PROJECT_ADMIN_ONE.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(PROJECT_ADMIN_ONE, project);
    }

    @Test
    public void When_CountCommonUserProjects_Expect_ReturnTwo() throws AbstractException {
        final int count = service.count(COMMON_USER_ONE.getId());
        Assert.assertEquals(2, count);
    }

    @Test
    public void When_ExistsByIdExistedProject_Expected_ReturnTrue() throws AbstractFieldException {
        final boolean exists = service.existsById(PROJECT_USER_ONE.getId());
        Assert.assertTrue(exists);
    }

    @Test
    public void When_ExistsByIdNotExistedProject_Expected_ReturnFalse() throws AbstractFieldException {
        final boolean exists = service.existsById(PROJECT_ID_NOT_EXISTED);
        Assert.assertFalse(exists);
    }

    @Test
    public void When_ExistsByIdWithUserIdExistedProject_Expected_ReturnTrue() throws AbstractFieldException {
        final boolean exists = service.existsById(COMMON_USER_ONE.getId(), PROJECT_USER_ONE.getId());
        Assert.assertTrue(exists);
    }

    @Test
    public void When_ExistsByIdWithUserIdNotExistedProject_Expected_ReturnFalse() throws AbstractFieldException {
        final boolean exists = service.existsById(COMMON_USER_ONE.getId(), PROJECT_ID_NOT_EXISTED);
        Assert.assertFalse(exists);
    }

    @Test
    public void When_FindAllUserId_Expected_ReturnListProjects() throws UserIdEmptyException {
        final List<Project> projects = service.findAll(COMMON_USER_ONE.getId());
        Assert.assertArrayEquals(projects.toArray(), USER_PROJECT_LIST.toArray());
    }

    @Test
    public void When_FindAllUserIdSorted_Expect_ReturnSortedProjectList() throws UserIdEmptyException {
        @NotNull final List<Project> projects = service.findAll(COMMON_USER_ONE.getId(), Sort.BY_NAME.getComparator());
        Assert.assertArrayEquals(projects.toArray(), USER_PROJECT_SORTED_LIST.toArray());
    }

    @Test
    public void When_RemoveExistedProject_Expect_ReturnProject() throws AbstractException {
        Assert.assertNotNull(service.save(ADMIN_USER_ONE.getId(), PROJECT_ADMIN_TWO));
        Assert.assertNotNull(service.remove(ADMIN_USER_ONE.getId(), PROJECT_ADMIN_TWO));
    }

    @Test
    public void When_RemoveNotProject_Expect_ThrowsEntityNotFoundException() {
        Assert.assertThrows(
                EntityNotFoundException.class,
                () -> service.remove(ADMIN_USER_ONE.getId(), PROJECT_NOT_EXISTED)
        );
    }

    @Test
    public void When_RemoveAll_Expect_ZeroCountProjects() throws AbstractException {
        service.save(PROJECT_ADMIN_ONE);
        service.save(PROJECT_ADMIN_TWO);
        service.removeAll(ADMIN_USER_ONE.getId());
        Assert.assertEquals(0, service.count(ADMIN_USER_ONE.getId()));
    }

    @Test
    public void When_RemoveByIdExistedProject_Expect_Project() throws AbstractException {
        Assert.assertNotNull(service.save(ADMIN_USER_ONE.getId(), PROJECT_ADMIN_TWO));
        Assert.assertNotNull(service.removeById(ADMIN_USER_ONE.getId(), PROJECT_ADMIN_TWO.getId()));
    }

    @Test
    public void When_RemoveByIdNotExistedProject_Expect_ThrowsEntityNotFoundException() {
        Assert.assertThrows(
                EntityNotFoundException.class,
                () -> service.removeById(ADMIN_USER_ONE.getId(), PROJECT_NOT_EXISTED.getId())
        );
    }

    @Test
    public void When_CreateNameProject_Expect_ExistedProject() throws AbstractFieldException {
        @Nullable final Project project = service.create(ADMIN_USER_ONE.getId(), NAME);
        Assert.assertNotNull(project);
        Assert.assertEquals(NAME, project.getName());
    }

    @Test
    public void When_CreateNameDescriptionProject_Expect_ExistedProject() throws AbstractFieldException {
        @Nullable final Project project = service.create(ADMIN_USER_ONE.getId(), NAME, DESCRIPTION);
        Assert.assertNotNull(project);
        Assert.assertEquals(NAME, project.getName());
        Assert.assertEquals(DESCRIPTION, project.getDescription());
    }

    @Test
    public void When_UpdateByIdProject_Expect_UpdatedProject() throws AbstractException {
        service.save(PROJECT_ADMIN_TWO);
        @Nullable Project project = service.updateById(
                PROJECT_ADMIN_TWO.getUserId(), PROJECT_ADMIN_TWO.getId(), NAME, DESCRIPTION
        );
        Assert.assertEquals(NAME, project.getName());
        Assert.assertEquals(DESCRIPTION, project.getDescription());
    }

}
