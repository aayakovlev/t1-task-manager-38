package ru.t1.aayakovlev.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.aayakovlev.tm.enumerated.Sort;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;
import ru.t1.aayakovlev.tm.exception.field.UserIdEmptyException;
import ru.t1.aayakovlev.tm.marker.UnitCategory;
import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.repository.impl.TaskRepositoryImpl;
import ru.t1.aayakovlev.tm.service.ConnectionService;
import ru.t1.aayakovlev.tm.service.PropertyService;
import ru.t1.aayakovlev.tm.service.TaskService;
import ru.t1.aayakovlev.tm.service.impl.ConnectionServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.PropertyServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.TaskServiceImpl;

import java.sql.Connection;
import java.util.List;

import static ru.t1.aayakovlev.tm.constant.TaskTestConstant.*;
import static ru.t1.aayakovlev.tm.constant.UserTestConstant.ADMIN_USER_ONE;
import static ru.t1.aayakovlev.tm.constant.UserTestConstant.COMMON_USER_ONE;
import static ru.t1.aayakovlev.tm.constant.ProjectTestConstant.PROJECT_ID_NOT_EXISTED;
import static ru.t1.aayakovlev.tm.constant.ProjectTestConstant.PROJECT_USER_ONE;

@Category(UnitCategory.class)
public final class TaskRepositoryImplTest {

    @NotNull
    private static final PropertyService propertyService = new PropertyServiceImpl();

    @NotNull
    private static final ConnectionService connectionService = new ConnectionServiceImpl(propertyService);

    @NotNull
    private static final Connection connection = connectionService.getConnection();

    @NotNull
    private static final TaskRepository repository = new TaskRepositoryImpl(connectionService.getConnection());

    @NotNull
    private static final TaskService service = new TaskServiceImpl(connectionService);

    @Before
    public void init() throws EntityEmptyException {
        service.save(TASK_USER_ONE);
        service.save(TASK_USER_TWO);
    }

    @After
    public void after() {
        service.removeAll(TASK_LIST);
    }

    @Test
    public void When_FindByIdExistsTask_Expect_ReturnTask() throws AbstractException {
        @Nullable final Task task = repository.findById(COMMON_USER_ONE.getId(), TASK_USER_ONE.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK_USER_ONE.getDescription(), task.getDescription());
        Assert.assertEquals(TASK_USER_ONE.getName(), task.getName());
        Assert.assertEquals(TASK_USER_ONE.getStatus(), task.getStatus());
        Assert.assertEquals(TASK_USER_ONE.getUserId(), task.getUserId());
        Assert.assertEquals(TASK_USER_ONE.getCreated(), task.getCreated());
    }

    @Test
    public void When_FindByIdExistsTask_Expect_ReturnNull() throws AbstractException {
        @Nullable final Task task = repository.findById(TASK_ID_NOT_EXISTED);
        Assert.assertNull(task);
    }

    @Test
    @SneakyThrows
    public void When_SaveNotNullTask_Expect_ReturnTask() {
        @NotNull final Task savedTask = repository.save(ADMIN_USER_ONE.getId(), TASK_ADMIN_ONE);
        connection.commit();
        Assert.assertNotNull(savedTask);
        Assert.assertEquals(TASK_ADMIN_ONE, savedTask);
        @Nullable final Task task = repository.findById(ADMIN_USER_ONE.getId(), TASK_ADMIN_ONE.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(TASK_ADMIN_ONE.getId(), task.getId());
    }

    @Test
    public void When_CountCommonUserTasks_Expect_ReturnTwo() throws AbstractException {
        final int count = repository.count(COMMON_USER_ONE.getId());
        Assert.assertEquals(2, count);
    }

    @Test
    public void When_ExistsByIdExistedTask_Expected_ReturnTrue() throws AbstractFieldException {
        final boolean exists = repository.existsById(TASK_USER_ONE.getId());
        Assert.assertTrue(exists);
    }

    @Test
    public void When_ExistsByIdNotExistedTask_Expected_ReturnFalse() throws AbstractFieldException {
        final boolean exists = repository.existsById(TASK_ID_NOT_EXISTED);
        Assert.assertFalse(exists);
    }

    @Test
    public void When_ExistsByIdWithUserIdExistedTask_Expected_ReturnTrue() throws AbstractFieldException {
        final boolean exists = repository.existsById(COMMON_USER_ONE.getId(), TASK_USER_ONE.getId());
        Assert.assertTrue(exists);
    }

    @Test
    public void When_ExistsByIdWithUserIdNotExistedTask_Expected_ReturnFalse() throws AbstractFieldException {
        final boolean exists = repository.existsById(COMMON_USER_ONE.getId(), TASK_ID_NOT_EXISTED);
        Assert.assertFalse(exists);
    }

    @Test
    public void When_FindAllUserId_Expected_ReturnListTasks() throws UserIdEmptyException {
        final List<Task> tasks = repository.findAll(COMMON_USER_ONE.getId());
        for (int i = 0; i < tasks.size(); i++) {
            Assert.assertEquals(USER_TASK_LIST.get(i).getName(), tasks.get(i).getName());
        }
    }

    @Test
    public void When_FindAllUserIdSorted_Expect_ReturnSortedTaskList() throws UserIdEmptyException {
        @NotNull final List<Task> tasks = repository.findAll(COMMON_USER_ONE.getId(), Sort.BY_NAME.getComparator());
        for (int i = 0; i < tasks.size(); i++) {
            Assert.assertEquals(USER_TASK_SORTED_LIST.get(i).getName(), tasks.get(i).getName());
        }
    }

    @Test
    @SneakyThrows
    public void When_RemoveExistedTask_Expect_ReturnTask() {
        @Nullable Task task1 = repository.save(ADMIN_USER_ONE.getId(), TASK_ADMIN_TWO);
        connection.commit();
        Assert.assertNotNull(task1);
        @Nullable Task task2 = repository.remove(ADMIN_USER_ONE.getId(), TASK_ADMIN_TWO);
        connection.commit();
        Assert.assertNotNull(task2);
    }

    @Test
    public void When_RemoveNotTask_Expect_ThrowsEntityNotFoundException() {
        Assert.assertThrows(
                EntityNotFoundException.class,
                () -> repository.remove(ADMIN_USER_ONE.getId(), TASK_NOT_EXISTED)
        );
    }

    @Test
    @SneakyThrows
    public void When_RemoveAll_Expect_ZeroCountTasks() {
        repository.save(TASK_ADMIN_ONE);
        connection.commit();
        repository.save(TASK_ADMIN_TWO);
        connection.commit();
        repository.removeAll(ADMIN_USER_ONE.getId());
        connection.commit();
        Assert.assertEquals(0, repository.count(ADMIN_USER_ONE.getId()));
    }

    @Test
    @SneakyThrows
    public void When_RemoveByIdExistedTask_Expect_Task() {
        @Nullable Task task1 = repository.save(ADMIN_USER_ONE.getId(), TASK_ADMIN_TWO);
        connection.commit();
        Assert.assertNotNull(task1);
        @Nullable Task task2 = repository.removeById(ADMIN_USER_ONE.getId(), TASK_ADMIN_TWO.getId());
        connection.commit();
        Assert.assertNotNull(task2);
    }

    @Test
    public void When_RemoveByIdNotExistedTask_Expect_ThrowsEntityNotFoundException() {
        Assert.assertThrows(
                EntityNotFoundException.class,
                () -> repository.removeById(ADMIN_USER_ONE.getId(), TASK_NOT_EXISTED.getId())
        );
    }

    @Test
    @SneakyThrows
    public void When_CreateNameTask_Expect_ExistedTask() {
        @Nullable final Task task = repository.create(ADMIN_USER_ONE.getId(), NAME);
        connection.commit();
        Assert.assertNotNull(task);
        Assert.assertEquals(NAME, task.getName());
    }

    @Test
    @SneakyThrows
    public void When_CreateNameDescriptionTask_Expect_ExistedTask() {
        @Nullable final Task task = repository.create(ADMIN_USER_ONE.getId(), NAME, DESCRIPTION);
        connection.commit();
        Assert.assertNotNull(task);
        Assert.assertEquals(NAME, task.getName());
        Assert.assertEquals(DESCRIPTION, task.getDescription());
    }

    @Test
    public void When_FindAllByProjectIdExistedProject_Expect_ReturnTaskList() {
        @NotNull final List<Task> tasks = repository.findAllByProjectId(COMMON_USER_ONE.getId(), PROJECT_USER_ONE.getId());
        Assert.assertNotEquals(0, tasks.size());
    }

    @Test
    public void When_FindAllByProjectIdNotExistedProject_Expect_ReturnEmptyList() {
        @NotNull final List<Task> tasks = repository.findAllByProjectId(COMMON_USER_ONE.getId(), PROJECT_ID_NOT_EXISTED);
        Assert.assertEquals(0, tasks.size());
    }

}
