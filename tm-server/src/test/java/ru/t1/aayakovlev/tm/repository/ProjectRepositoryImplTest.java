package ru.t1.aayakovlev.tm.repository;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.aayakovlev.tm.enumerated.Sort;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;
import ru.t1.aayakovlev.tm.exception.field.UserIdEmptyException;
import ru.t1.aayakovlev.tm.marker.UnitCategory;
import ru.t1.aayakovlev.tm.model.Project;
import ru.t1.aayakovlev.tm.repository.impl.ProjectRepositoryImpl;
import ru.t1.aayakovlev.tm.service.ConnectionService;
import ru.t1.aayakovlev.tm.service.ProjectService;
import ru.t1.aayakovlev.tm.service.PropertyService;
import ru.t1.aayakovlev.tm.service.impl.ConnectionServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.ProjectServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.PropertyServiceImpl;

import java.sql.Connection;
import java.util.List;

import static ru.t1.aayakovlev.tm.constant.ProjectTestConstant.*;
import static ru.t1.aayakovlev.tm.constant.UserTestConstant.ADMIN_USER_ONE;
import static ru.t1.aayakovlev.tm.constant.UserTestConstant.COMMON_USER_ONE;

@Category(UnitCategory.class)
public final class ProjectRepositoryImplTest {

    @NotNull
    private static final PropertyService propertyService = new PropertyServiceImpl();

    @NotNull
    private static final ConnectionService connectionService = new ConnectionServiceImpl(propertyService);

    @NotNull
    private static final Connection connection = connectionService.getConnection();

    @NotNull
    private static final ProjectRepository repository = new ProjectRepositoryImpl(connection);

    @NotNull
    private static final ProjectService service = new ProjectServiceImpl(connectionService);

    @Before
    public void init() throws EntityEmptyException {
        service.save(PROJECT_USER_ONE);
        service.save(PROJECT_USER_TWO);
    }

    @After
    public void after() {
        service.removeAll(PROJECT_LIST);
    }

    @Test
    public void When_FindByIdExistsProject_Expect_ReturnProject() throws AbstractException {
        @Nullable final Project project = repository.findById(COMMON_USER_ONE.getId(), PROJECT_USER_ONE.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(PROJECT_USER_ONE.getDescription(), project.getDescription());
        Assert.assertEquals(PROJECT_USER_ONE.getName(), project.getName());
        Assert.assertEquals(PROJECT_USER_ONE.getStatus(), project.getStatus());
        Assert.assertEquals(PROJECT_USER_ONE.getUserId(), project.getUserId());
        Assert.assertEquals(PROJECT_USER_ONE.getCreated(), project.getCreated());
    }

    @Test
    public void When_FindByIdExistsProject_Expect_ReturnNull() throws AbstractException {
        @Nullable final Project project = repository.findById(PROJECT_ID_NOT_EXISTED);
        Assert.assertNull(project);
    }

    @Test
    @SneakyThrows
    public void When_SaveNotNullProject_Expect_ReturnProject() {
        @NotNull final Project savedProject = repository.save(ADMIN_USER_ONE.getId(), PROJECT_ADMIN_ONE);
        connection.commit();
        Assert.assertNotNull(savedProject);
        Assert.assertEquals(PROJECT_ADMIN_ONE, savedProject);
        @Nullable final Project project = repository.findById(ADMIN_USER_ONE.getId(), PROJECT_ADMIN_ONE.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(PROJECT_ADMIN_ONE.getDescription(), project.getDescription());
        Assert.assertEquals(PROJECT_ADMIN_ONE.getName(), project.getName());
        Assert.assertEquals(PROJECT_ADMIN_ONE.getStatus(), project.getStatus());
        Assert.assertEquals(PROJECT_ADMIN_ONE.getUserId(), project.getUserId());
        Assert.assertEquals(PROJECT_ADMIN_ONE.getCreated(), project.getCreated());
    }

    @Test
    public void When_CountCommonUserProjects_Expect_ReturnTwo() throws AbstractException {
        final int count = repository.count(COMMON_USER_ONE.getId());
        Assert.assertEquals(2, count);
    }

    @Test
    public void When_ExistsByIdExistedProject_Expected_ReturnTrue() throws AbstractFieldException {
        final boolean exists = repository.existsById(PROJECT_USER_ONE.getId());
        Assert.assertTrue(exists);
    }

    @Test
    public void When_ExistsByIdNotExistedProject_Expected_ReturnFalse() throws AbstractFieldException {
        final boolean exists = repository.existsById(PROJECT_ID_NOT_EXISTED);
        Assert.assertFalse(exists);
    }

    @Test
    public void When_ExistsByIdWithUserIdExistedProject_Expected_ReturnTrue() throws AbstractFieldException {
        final boolean exists = repository.existsById(COMMON_USER_ONE.getId(), PROJECT_USER_ONE.getId());
        Assert.assertTrue(exists);
    }

    @Test
    public void When_ExistsByIdWithUserIdNotExistedProject_Expected_ReturnFalse() throws AbstractFieldException {
        final boolean exists = repository.existsById(COMMON_USER_ONE.getId(), PROJECT_ID_NOT_EXISTED);
        Assert.assertFalse(exists);
    }

    @Test
    public void When_FindAllUserId_Expected_ReturnListProjects() throws UserIdEmptyException {
        final List<Project> projects = repository.findAll(COMMON_USER_ONE.getId());
        for (int i = 0; i < projects.size(); i++) {
            Assert.assertEquals(USER_PROJECT_LIST.get(i).getName(), projects.get(i).getName());
        }
    }

    @Test
    public void When_FindAllUserIdSorted_Expect_ReturnSortedProjectList() throws UserIdEmptyException {
        @NotNull final List<Project> projects = repository.findAll(COMMON_USER_ONE.getId(), Sort.BY_NAME.getComparator());
        for (int i = 0; i < projects.size(); i++) {
            Assert.assertEquals(USER_PROJECT_SORTED_LIST.get(i).getName(), projects.get(i).getName());
        }
    }

    @Test
    @SneakyThrows
    public void When_RemoveExistedProject_Expect_ReturnProject() throws AbstractException {
        @Nullable Project project1 = repository.save(ADMIN_USER_ONE.getId(), PROJECT_ADMIN_TWO);
        connection.commit();
        Assert.assertNotNull(project1);
        @Nullable Project project2 = repository.remove(ADMIN_USER_ONE.getId(), PROJECT_ADMIN_TWO);
        connection.commit();
        Assert.assertNotNull(project2);
    }

    @Test
    public void When_RemoveNotProject_Expect_ThrowsEntityNotFoundException() {
        Assert.assertThrows(
                EntityNotFoundException.class,
                () -> repository.remove(ADMIN_USER_ONE.getId(), PROJECT_NOT_EXISTED)
        );
    }

    @Test
    @SneakyThrows
    public void When_RemoveAll_Expect_ZeroCountProjects() {
        repository.save(PROJECT_ADMIN_ONE);
        connection.commit();
        repository.save(PROJECT_ADMIN_TWO);
        connection.commit();
        repository.removeAll(ADMIN_USER_ONE.getId());
        connection.commit();
        Assert.assertEquals(0, repository.count(ADMIN_USER_ONE.getId()));
    }

    @Test
    @SneakyThrows
    public void When_RemoveByIdExistedProject_Expect_Project() {
        Assert.assertNotNull(repository.save(ADMIN_USER_ONE.getId(), PROJECT_ADMIN_TWO));
        connection.commit();
        @Nullable Project project = repository.removeById(ADMIN_USER_ONE.getId(), PROJECT_ADMIN_TWO.getId());
        connection.commit();
        Assert.assertNotNull(project);
    }

    @Test
    public void When_RemoveByIdNotExistedProject_Expect_ThrowsEntityNotFoundException() {
        Assert.assertThrows(
                EntityNotFoundException.class,
                () -> repository.removeById(ADMIN_USER_ONE.getId(), PROJECT_NOT_EXISTED.getId())
        );
    }

    @Test
    @SneakyThrows
    public void When_CreateNameProject_Expect_ExistedProject() {
        @Nullable final Project project = repository.create(ADMIN_USER_ONE.getId(), NAME);
        connection.commit();
        Assert.assertNotNull(project);
        Assert.assertEquals(NAME, project.getName());
    }

    @Test
    @SneakyThrows
    public void When_CreateNameDescriptionProject_Expect_ExistedProject() {
        @Nullable final Project project = repository.create(ADMIN_USER_ONE.getId(), NAME, DESCRIPTION);
        connection.commit();
        Assert.assertNotNull(project);
        Assert.assertEquals(NAME, project.getName());
        Assert.assertEquals(DESCRIPTION, project.getDescription());
    }

}
