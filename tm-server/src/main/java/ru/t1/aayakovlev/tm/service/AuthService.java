package ru.t1.aayakovlev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.model.Session;
import ru.t1.aayakovlev.tm.model.User;

public interface AuthService {

    @NotNull
    Session validateToken(@Nullable final String token);

    void invalidate(@Nullable final Session session);

    @NotNull
    String login(
            @Nullable final String login,
            @Nullable final String password
    ) throws AbstractException;

    @NotNull
    User registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws AbstractException;

    @NotNull
    User profile(@Nullable final String userId) throws AbstractException;

}
