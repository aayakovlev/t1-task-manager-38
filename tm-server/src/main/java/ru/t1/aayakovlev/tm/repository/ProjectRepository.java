package ru.t1.aayakovlev.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.model.Project;

public interface ProjectRepository extends UserOwnedRepository<Project> {

    @NotNull
    Project create(@NotNull final String userId, @NotNull final String name);

    @NotNull
    Project create(@NotNull final String userId, @NotNull final String name, @NotNull final String description);

    @NotNull
    Project update(@NotNull final Project project);

}
