package ru.t1.aayakovlev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;
import ru.t1.aayakovlev.tm.exception.field.UserIdEmptyException;
import ru.t1.aayakovlev.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface UserOwnedRepository<M extends AbstractUserOwnedModel> extends BaseRepository<M> {

    void clear(@NotNull final String userId) throws UserIdEmptyException;

    int count(@NotNull final String userId) throws UserIdEmptyException;

    boolean existsById(@NotNull final String userId, @NotNull final String id) throws AbstractFieldException;

    @NotNull
    List<M> findAll(@NotNull final String userId) throws UserIdEmptyException;

    @NotNull
    List<M> findAll(@NotNull final String userId, @NotNull final Comparator<M> comparator) throws UserIdEmptyException;

    @Nullable
    M findById(@NotNull final String userId, @NotNull final String id) throws AbstractException;

    @NotNull
    M remove(@NotNull final String userId, @NotNull final M model) throws AbstractException;

    void removeAll(@NotNull final String userId) throws AbstractException;

    @NotNull
    M removeById(@NotNull final String userId, @NotNull final String id) throws AbstractException;

    @NotNull
    M save(@NotNull final String userId, @NotNull final M model) throws AbstractException;

}
