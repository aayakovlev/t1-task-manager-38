package ru.t1.aayakovlev.tm.service;

import org.jetbrains.annotations.NotNull;

public interface DatabaseProperty {

    @NotNull
    String getDatabaseUser();

    @NotNull
    String getDatabasePassword();

    @NotNull
    String getDatabaseURL();

}
