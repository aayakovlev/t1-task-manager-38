package ru.t1.aayakovlev.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.model.Session;

public interface SessionRepository extends UserOwnedRepository<Session> {

    @NotNull
    Session update(@NotNull final Session session);

}
