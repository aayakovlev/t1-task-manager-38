package ru.t1.aayakovlev.tm.service.impl;

import lombok.SneakyThrows;
import org.apache.commons.dbcp2.BasicDataSource;
import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.service.ConnectionService;
import ru.t1.aayakovlev.tm.service.DatabaseProperty;

import java.sql.Connection;

public final class ConnectionServiceImpl implements ConnectionService {

    @NotNull
    private final DatabaseProperty databaseProperty;

    public ConnectionServiceImpl(@NotNull final DatabaseProperty databaseProperty) {
        this.databaseProperty = databaseProperty;
    }

    @NotNull
    public static BasicDataSource dataSource(
            @NotNull final String url,
            @NotNull final String username,
            @NotNull final String password
    ) {
        @NotNull final BasicDataSource dataSource = new BasicDataSource();
        dataSource.setUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        dataSource.setMinIdle(5);
        dataSource.setMaxIdle(10);
        dataSource.setMaxOpenPreparedStatements(100);
        dataSource.setMaxConnLifetimeMillis(5000);
        dataSource.setAutoCommitOnReturn(false);
        return dataSource;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Connection getConnection() {
        @NotNull final String username = databaseProperty.getDatabaseUser();
        @NotNull final String password = databaseProperty.getDatabasePassword();
        @NotNull final String url = databaseProperty.getDatabaseURL();
        @NotNull final Connection connection = dataSource(url, username, password).getConnection();
        connection.setAutoCommit(false);
        return connection;
    }

}
