package ru.t1.aayakovlev.tm.repository.impl;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.model.User;
import ru.t1.aayakovlev.tm.repository.UserRepository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public final class UserRepositoryImpl extends AbstractBaseRepository<User> implements UserRepository {

    @NotNull
    private final static String TABLE_NAME = "public.user";

    public UserRepositoryImpl(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @NotNull
    @Override
    @SneakyThrows
    protected User fetch(@NotNull final ResultSet row) {
        @NotNull final User user = new User();
        user.setId(row.getString("id"));
        user.setFirstName(row.getString("first_name"));
        user.setLastName(row.getString("last_name"));
        user.setMiddleName(row.getString("middle_name"));
        user.setLogin(row.getString("login"));
        user.setPasswordHash(row.getString("password_hash"));
        user.setRole(Role.valueOf(row.getString("role")));
        user.setEmail(row.getString("email"));
        user.setLocked(row.getBoolean("locked"));
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User save(@NotNull final User user) {
        @NotNull final String sql = String.format(
                "insert into %s (id, login, password_hash, email, role, locked, first_name, last_name, middle_name) " +
                        "values (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getId());
            statement.setString(2, user.getLogin());
            statement.setString(3, user.getPasswordHash());
            statement.setString(4, user.getEmail());
            statement.setString(5, user.getRole().toString());
            statement.setBoolean(6, user.isLocked());
            statement.setString(7, user.getFirstName());
            statement.setString(8, user.getLastName());
            statement.setString(9, user.getMiddleName());
            statement.executeUpdate();
        }
        return user;
    }

    @Override
    @Nullable
    @SneakyThrows
    public User findByLogin(@NotNull final String login) {
        @NotNull final String sql = String.format("select * from %s where login = ? limit 1", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

    @Override
    @Nullable
    @SneakyThrows
    public User findByEmail(@NotNull final String email) {
        @NotNull final String sql = String.format("select * from %s where email = ? limit 1", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, email);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

    @Override
    public boolean isLoginExists(@NotNull final String login) {
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExists(@NotNull final String email) {
        return findByEmail(email) != null;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User update(@NotNull final User user) {
        @NotNull final String sql = String.format(
                "update %s set first_name = ?, last_name = ?, middle_name = ?, password_hash = ?, locked = ? " +
                        "where id = ?", getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getFirstName());
            statement.setString(2, user.getLastName());
            statement.setString(3, user.getMiddleName());
            statement.setString(4, user.getPasswordHash());
            statement.setBoolean(5, user.isLocked());
            statement.executeUpdate();
        }
        return user;
    }

}
