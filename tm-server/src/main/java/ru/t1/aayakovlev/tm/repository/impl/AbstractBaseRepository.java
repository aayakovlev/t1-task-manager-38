package ru.t1.aayakovlev.tm.repository.impl;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.comparator.CreatedComparator;
import ru.t1.aayakovlev.tm.comparator.NameComparator;
import ru.t1.aayakovlev.tm.comparator.StatusComparator;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;
import ru.t1.aayakovlev.tm.model.AbstractModel;
import ru.t1.aayakovlev.tm.repository.BaseRepository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

@NoArgsConstructor
public abstract class AbstractBaseRepository<M extends AbstractModel> implements BaseRepository<M> {

    @NotNull
    protected Connection connection;

    public AbstractBaseRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @NotNull
    protected abstract String getTableName();

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        if (comparator == StatusComparator.INSTANCE) return "status";
        if (comparator == NameComparator.INSTANCE) return "name";
        return "id";
    }

    @NotNull
    protected abstract M fetch(@NotNull final ResultSet row);

    @NotNull
    public abstract M save(@NotNull final M model);

    @Override
    @NotNull
    public Collection<M> add(@NotNull final Collection<M> models) {
        List<M> result = new ArrayList<>();
        for (M model : models) {
            result.add(save(model));
        }
        return result;
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final String sql = String.format("delete from %s", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.executeUpdate();
        }
    }

    @Override
    public boolean existsById(@NotNull final String id) throws AbstractFieldException {
        return findById(id) != null;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll() {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format("select * from %s", getTableName());
        try (@NotNull final Statement statement = connection.createStatement();
             @NotNull final ResultSet resultSet = statement.executeQuery(sql)) {
            while (resultSet.next()) result.add(fetch(resultSet));
        }
        return result;
    }

    @Override
    @Nullable
    @SneakyThrows
    public M findById(@NotNull final String id) throws AbstractFieldException {
        @NotNull final String sql = String.format("select * from %s where id = ? limit 1", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public M remove(@NotNull final M model) {
        @NotNull final String sql = String.format("delete from %s where id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.executeUpdate();
        }
        return model;
    }

    @Override
    public void removeAll(@NotNull final List<M> models) {
        for (M model : models) {
            remove(model);
        }
    }

    @Override
    @NotNull
    public M removeById(@NotNull final String id) throws AbstractException {
        final M model = findById(id);
        if (model == null) throw new EntityNotFoundException();
        return remove(model);
    }

    @Override
    @NotNull
    public Collection<M> set(@NotNull final Collection<M> models) {
        clear();
        return add(models);
    }

}
