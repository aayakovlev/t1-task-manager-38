package ru.t1.aayakovlev.tm.service.impl;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.exception.entity.ProjectNotFoundException;
import ru.t1.aayakovlev.tm.exception.entity.TaskNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.IdEmptyException;
import ru.t1.aayakovlev.tm.exception.field.ProjectIdEmptyException;
import ru.t1.aayakovlev.tm.exception.field.TaskIdEmptyException;
import ru.t1.aayakovlev.tm.exception.field.UserIdEmptyException;
import ru.t1.aayakovlev.tm.model.Project;
import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.repository.ProjectRepository;
import ru.t1.aayakovlev.tm.repository.TaskRepository;
import ru.t1.aayakovlev.tm.repository.impl.ProjectRepositoryImpl;
import ru.t1.aayakovlev.tm.repository.impl.TaskRepositoryImpl;
import ru.t1.aayakovlev.tm.service.ConnectionService;
import ru.t1.aayakovlev.tm.service.ProjectTaskService;

import java.sql.Connection;
import java.util.List;

public final class ProjectTaskServiceImpl implements ProjectTaskService {

    @NotNull
    private final ConnectionService connectionService;

    public ProjectTaskServiceImpl(@NotNull final ConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    private Connection getConnection() {
        return connectionService.getConnection();
    }

    @NotNull
    private ProjectRepository getProjectRepository(@NotNull Connection connection) {
        return new ProjectRepositoryImpl(connection);
    }

    @NotNull
    private TaskRepository getTaskRepository(@NotNull Connection connection) {
        return new TaskRepositoryImpl(connection);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ProjectRepository projectRepository = getProjectRepository(connection);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            @NotNull final TaskRepository taskRepository = getTaskRepository(connection);
            @Nullable final Task task = taskRepository.findById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(projectId);
            taskRepository.update(task);
            connection.commit();
            return task;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ProjectRepository projectRepository = getProjectRepository(connection);
            if (!projectRepository.existsById(userId, projectId)) throw new EntityNotFoundException();
            @NotNull final TaskRepository taskRepository = getTaskRepository(connection);
            @NotNull final List<Task> tasks = taskRepository.findAllByProjectId(userId, projectId);
            for (@NotNull final Task task : tasks) taskRepository.removeById(userId, task.getId());
            @NotNull final Project project = projectRepository.removeById(userId, projectId);
            connection.commit();
            return project;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ProjectRepository projectRepository = getProjectRepository(connection);
            if (!projectRepository.existsById(userId, projectId)) throw new EntityNotFoundException();
            @NotNull final TaskRepository taskRepository = getTaskRepository(connection);
            @Nullable final Task task = taskRepository.findById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            taskRepository.update(task);
            connection.commit();
            return task;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
