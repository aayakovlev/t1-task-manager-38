package ru.t1.aayakovlev.tm.service.impl;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.model.Session;
import ru.t1.aayakovlev.tm.repository.BaseRepository;
import ru.t1.aayakovlev.tm.repository.SessionRepository;
import ru.t1.aayakovlev.tm.repository.UserOwnedRepository;
import ru.t1.aayakovlev.tm.repository.impl.SessionRepositoryImpl;
import ru.t1.aayakovlev.tm.repository.impl.UserRepositoryImpl;
import ru.t1.aayakovlev.tm.service.ConnectionService;
import ru.t1.aayakovlev.tm.service.SessionService;

import java.sql.Connection;

public final class SessionServiceImpl extends AbstractUserOwnedService<Session> implements SessionService {

    public SessionServiceImpl(@NotNull final ConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    protected @NotNull SessionRepository getRepository(@NotNull final Connection connection) {
        return new SessionRepositoryImpl(connection);
    }

}
