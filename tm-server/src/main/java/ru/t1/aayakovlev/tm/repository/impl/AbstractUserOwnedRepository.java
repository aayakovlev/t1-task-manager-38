package ru.t1.aayakovlev.tm.repository.impl;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;
import ru.t1.aayakovlev.tm.model.AbstractUserOwnedModel;
import ru.t1.aayakovlev.tm.repository.UserOwnedRepository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractBaseRepository<M>
        implements UserOwnedRepository<M> {

    public AbstractUserOwnedRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Override
    @NotNull
    public abstract M save(@NotNull final String userId, @NotNull final M model);

    @Override
    public void clear(@NotNull final String userId) {
        final List<M> models = findAll(userId);
        removeAll(models);
    }

    @Override
    public int count(@NotNull final String userId) {
        return findAll(userId).size();
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) throws AbstractFieldException {
        return findById(userId, id) != null;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@NotNull final String userId) {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format("Select * from %s where user_id = ?", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            while (rowSet.next()) result.add(fetch(rowSet));
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@NotNull final String userId, @NotNull final Comparator<M> comparator) {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format(
                "Select * from %s where user_id = ? order by %s",
                getTableName(), getSortType(comparator)
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            while (rowSet.next()) result.add(fetch(rowSet));
        }
        return result;
    }

    @Override
    @Nullable
    @SneakyThrows
    public M findById(@NotNull final String userId, @NotNull final String id) throws AbstractFieldException {
        @NotNull final String sql = String.format("select * from %s where id = ? and user_id = ? limit 1", getTableName());
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            statement.setString(2, userId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

    @Override
    @NotNull
    public M remove(@NotNull final String userId, @NotNull final M model) throws AbstractException {
        return removeById(userId, model.getId());
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        final List<M> models = findAll(userId);
        removeAll(models);
    }

    @Override
    @NotNull
    public M removeById(@NotNull final String userId, @NotNull final String id) throws AbstractException {
        @Nullable final M model = findById(userId, id);
        if (model == null) throw new EntityNotFoundException();
        return remove(model);
    }

}
