package ru.t1.aayakovlev.tm.service;

import org.jetbrains.annotations.NotNull;

import java.sql.Connection;

public interface ConnectionService {

    @NotNull
    Connection getConnection();

}
