package ru.t1.aayakovlev.tm.repository.impl;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.repository.TaskRepository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public final class TaskRepositoryImpl extends AbstractUserOwnedRepository<Task> implements TaskRepository {

    @NotNull
    private final static String TABLE_NAME = "public.task";

    public TaskRepositoryImpl(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    @NotNull
    public Task create(@NotNull final String userId, @NotNull final String name) {
        final Task task = new Task();
        task.setName(name);
        return save(userId, task);
    }

    @Override
    @NotNull
    public Task create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return save(userId, task);
    }

    @NotNull
    @Override
    @SneakyThrows
    protected Task fetch(@NotNull final ResultSet row) {
        @NotNull final Task task = new Task();
        task.setId(row.getString("id"));
        task.setName(row.getString("name"));
        task.setDescription(row.getString("description"));
        task.setUserId(row.getString("user_id"));
        task.setProjectId(row.getString("project_id"));
        task.setStatus(Status.valueOf(row.getString("status")));
        task.setCreated(row.getTimestamp("created"));
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final String sql = String.format(
                "select * from %s where project_id = ? and user_id = ?", getTableName()
        );
        @NotNull final List<Task> result = new ArrayList<>();
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, projectId);
            statement.setString(2, userId);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            while (rowSet.next()) result.add(fetch(rowSet));
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task save(@NotNull final Task task) {
        @NotNull final String sql = String.format(
                "insert into %s (id, created, name, description, status, project_id, user_id) values (?, ?, ?, ?, ?, ?, ?)",
                getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getId());
            statement.setTimestamp(2, new Timestamp(task.getCreated().getTime()));
            statement.setString(3, task.getName());
            statement.setString(4, task.getDescription());
            statement.setString(5, task.getStatus().toString());
            statement.setString(6, task.getProjectId());
            statement.setString(7, task.getUserId());
            statement.executeUpdate();
        }
        return task;
    }

    @NotNull
    @Override
    public Task save(@NotNull final String userId, @NotNull final Task task) {
        task.setUserId(userId);
        return save(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task update(@NotNull final Task task) {
        @NotNull final String sql = String.format(
                "update %s set name = ?, description = ?, status = ?, project_id = ? where id = ?", getTableName()
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getName());
            statement.setString(2, task.getDescription());
            statement.setString(3, task.getStatus().toString());
            statement.setString(4, task.getProjectId());
            statement.executeUpdate();
        }
        return task;
    }

}
