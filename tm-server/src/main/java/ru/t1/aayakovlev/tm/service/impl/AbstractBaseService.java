package ru.t1.aayakovlev.tm.service.impl;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;
import ru.t1.aayakovlev.tm.exception.field.IdEmptyException;
import ru.t1.aayakovlev.tm.model.AbstractModel;
import ru.t1.aayakovlev.tm.repository.BaseRepository;
import ru.t1.aayakovlev.tm.service.BaseService;
import ru.t1.aayakovlev.tm.service.ConnectionService;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public abstract class AbstractBaseService<M extends AbstractModel>
        implements BaseService<M> {

    @NotNull
    protected final ConnectionService connectionService;

    public AbstractBaseService(@NotNull final ConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    protected Connection getConnection() {
        return connectionService.getConnection();
    }

    @NotNull
    protected abstract BaseRepository<M> getRepository(@NotNull final Connection connection);

    @NotNull
    @Override
    @SneakyThrows
    public Collection<M> add(@NotNull final Collection<M> models) {
        @NotNull final Connection connection = getConnection();
        @Nullable final Collection<M> results;
        try {
            @NotNull final BaseRepository<M> repository = getRepository(connection);
            results = repository.add(models);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return results;
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final BaseRepository<M> repository = getRepository(connection);
            repository.clear();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String id) throws AbstractFieldException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final BaseRepository<M> repository = getRepository(connection);
            return repository.existsById(id);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll() {
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final BaseRepository<M> repository = getRepository(connection);
            return repository.findAll();
        }
    }


    @NotNull
    @Override
    @SneakyThrows
    public M findById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final BaseRepository<M> repository = getRepository(connection);
            @Nullable final M model = repository.findById(id);
            if (model == null) throw new EntityNotFoundException();
            return model;
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public M remove(@Nullable final M model) throws AbstractException {
        if (model == null) throw new EntityNotFoundException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final BaseRepository<M> repository = getRepository(connection);
            @NotNull final M result = repository.remove(model);
            connection.commit();
            return result;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeAll(@NotNull final List<M> models) {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final BaseRepository<M> repository = getRepository(connection);
            repository.removeAll(models);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public M removeById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final BaseRepository<M> repository = getRepository(connection);
            @NotNull final M result = repository.removeById(id);
            connection.commit();
            return result;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public M save(@Nullable final M model) throws EntityEmptyException {
        if (model == null) throw new EntityEmptyException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final BaseRepository<M> repository = getRepository(connection);
            @NotNull final M result = repository.save(model);
            connection.commit();
            return result;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<M> set(@NotNull final Collection<M> models) {
        if (models.isEmpty()) return Collections.emptyList();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final BaseRepository<M> repository = getRepository(connection);
            @NotNull final Collection<M> result = repository.set(models);
            connection.commit();
            return result;
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
